from PIL import Image, ImageFile
import imagehash


def isSamePic(image_file1, image_file2, max_dif=6):
    """
    max_dif: 允许最大hash差值, 越小越精确,最小为0
    推荐使用
    """
    ImageFile.LOAD_TRUNCATED_IMAGES = True
    hash_1 = None
    hash_2 = None
    with open(image_file1, 'rb') as fp:
        hash_1 = imagehash.average_hash(Image.open(fp))
        # print(hash_1)
    with open(image_file2, 'rb') as fp:
        hash_2 = imagehash.average_hash(Image.open(fp))
        # print(hash_2)
    dif = hash_1 - hash_2
    print('dif=', abs(dif), ', ', end='', sep='')
    if abs(dif) <= max_dif:
        return True
    else:
        return False
