from os import system
from time import sleep
from devices import devicesIP, devicesID
from config import pictureRoot
from os.path import exists
from shutil import rmtree
from os import mkdir


def clearUNIDir(deviceName):
    device = devicesIP[deviceName]
    cmd = 'adb -s %s shell rm -r /sdcard/Android/data/com.xinchidao.yxrw/apps/__UNI__2B5BA32' % device
    # print(cmd)
    print('清理临时文件夹__UNI__2B5BA32')
    system(cmd)
    # cmd = 'adb -s %s reboot' % devicesID[deviceName]
    # print('重启%s' % deviceName)
    # system(cmd)
    # sleep(16)
    if not deviceName == '001':
        return
    print('清理图片文件夹')
    if exists(pictureRoot):
        rmtree(pictureRoot)
    mkdir(pictureRoot)


def reconnectDevice(deviceName):
    dIP = devicesIP[deviceName]
    dID = devicesID[deviceName]
    cmds = ['adb -s %s tcpip 5555' % dID,
            'adb disconnect %s' % dIP,
            'adb connect %s' % dIP]
    for c in cmds:
        system(c)


def fixDevice(deviceName):
    dIP = devicesIP[deviceName]
    cmds = [
            ('adb -s %s shell input keyevent 3' % dIP, 1, '返回桌面'),
            ('adb -s %s shell input keyevent 82' % dIP, 1, '打开最近任务'),
            ('adb -s %s shell input tap 540 1706' % dIP, 1, '清理内存'),
            ('adb -s %s shell input keyevent 82' % dIP, 1, '打开最近任务'),
            ('adb -s %s shell input swipe 544 1331 553 582' % dIP, 1, '清理按键精灵'),
            ('adb -s %s shell input tap 540 1706' % dIP, 1, '清理内存'),
            ('adb -s %s shell rm -r /sdcard/Android/data/com.xinchidao.yxrw/apps/__UNI__2B5BA32' % dIP,
             1, '清理临时文件夹'),
            ('adb -s %s shell input tap 897 1798' % dIP, 9, '打开按键精灵'),
            ('adb -s %s shell input tap 480 1584' % dIP, 1, '启动悬浮窗'),
            ('adb -s %s shell input tap 1069 589' % dIP, 1, '展开悬浮窗'),
            ('adb -s %s shell input tap 405 598' % dIP, 1, '启动')]
    for c in cmds:
        print(c[2])
        system(c[0])
        sleep(c[1])


if __name__ == '__main__':
    fixDevice('018')
