from screencap import getScreen
from datetime import datetime
from sendEmail import sendAlarm, sendRecoveredEmail
from fix import fixDevice

st = datetime.now()


class ErrCnts():
    errCnts = [0 for i in range(100)]

    @classmethod
    def set(cls, index, value=-1):
        if value == -1:
            cls.errCnts[index] = cls.errCnts[index] + 1
        else:
            cls.errCnts[index] = value

    @classmethod
    def get(cls, index):
        return cls.errCnts[index]


def checkDevice(deviceName):
    deviceNum = int(deviceName)
    if getScreen(deviceName):
        ErrCnts.set(deviceNum)
    else:
        ErrCnts.set(deviceNum, 0)
    if ErrCnts.get(deviceNum) > 19:
        sendAlarm(deviceName)
        fixDevice(deviceName)
        sendRecoveredEmail(deviceName)
        ErrCnts.set(deviceNum, 0)
    else:
        errMax = max(ErrCnts.errCnts)
        print('正常, errCnt=%d, errNum=%03d, runTime=%s' %
              (errMax, ErrCnts.errCnts.index(errMax), datetime.now() - st), sep='')
