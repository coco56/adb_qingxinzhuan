# 待开发功能 #
# 1. 切换成按键控制模式
# 2. 增加异常后或定时重启，重连方案
# 3. 增加未连接自动提醒+修复方案（可通过截图是否正常保存来判断）

from check import checkDevice
from fix import clearUNIDir

dsName = [
    '001',
    '002',
    '004', '005', '006', '007', '008',
    '009', '010', '011', '012', '013', '014',
    '015', '016', '017', '018', '019']
cnt = 0

while True:
    cnt += 1
    for i in dsName:
        print('c=%d, p=' % cnt, end='')
        try:
            checkDevice(i)
        except TypeError as e:
            print(e)
        if not (cnt - 1) % 2000:
            clearUNIDir(i)
        print()
